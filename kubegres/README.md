Dokumentation von kubegres: https://www.kubegres.io/doc/getting-started.html 

## Installation des Operators

Wie in Anleitung beschrieben. Getestet:

    kubectl apply -f https://raw.githubusercontent.com/reactive-tech/kubegres/v1.15/kubegres.yaml

## Installation des Datenbankclusters

Um Konsistenz mit der restlichen Folio-Installation zu waren, werden helm templates verwendet. So wird eine 'global-values.yaml' mit eingebunden, die auch für die Installation von z.B. Okapi, den backend-modules etc. verwendet wird.

Allerdings scheint eine direkte Installation mit helm nicht über `helm upgrade` angepasst werden zu können.

Achtung: namespace in FOLIONS richtig setzen!
Installationsvorgang (vom main directory des repositories aus):
    helm template kubegres kubegres/ -f global-values.yaml -s templates/postgres.secret.yaml > kubegres/renders/secret.yaml
    kubectl -n ${FOLIONS:-undefined} apply -f kubegres/renders/secret.yaml
    helm template kubegres kubegres/ -f global-values.yaml -s templates/postgres.config.yaml > kubegres/renders/config.yaml
    kubectl -n ${FOLIONS:-undefined} apply -f kubegres/renders/config.yaml
    helm template kubegres kubegres/ -f global-values.yaml -s templates/postgres.kubegres.yaml > kubegres/renders/kubegres.yaml
    kubectl -n ${FOLIONS:-undefined} apply -f kubegres/renders/kubegres.yaml


## Anpassung der kubegres-resource

Um an der Kubegres-resource Anpassungen vorzunehmen, muss diese Deinstalliert werden:

    kubectl -n ${FOLIONS:-undefined} delete -f kubegres/renders/kubegres.yaml

Dabei bleiben die persistent volumes (PV) und persisten volume claims (PVC) des kubegres-Operators bestehen.

Diese werden auch wieder verwendet, zumindest wenn der Index der aktuell laufenden Pods 1-3 sind.

Anscheinend bewirkt der Reboot der Nodes (z.B. wegen Updates) das hochzählen der Postgres-Pod-Indexe.

Hier ist ein manueller Eingriff nötig. Um auch bei einem Fehler die Datenbanken nicht zu verlieren, bietet es sich an, die VolumeReclaimPolicy der relevanten PVs auf Retain zu setzen:

    kubectl -n ${FOLIONS:-undefined} patch pv pvc-<ID> -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'

Welche gerade in Pods verwendet werden kann man entweder im Dashboard herausfinden, oder in Longhorn (sollten State: Healthy haben).

Danach kann man die PVC von Index 1-3 auf die aktuell verwendeten PV setzen. Hierzu muss man:
* bei PV: spec.claimRef auf PVC setzen (Name UND uid!)
* bei PVC: spec.volumeName auf PV setzen
