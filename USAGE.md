## Vorbereitungen

#### Shell-Variable erstellen und Helm-Namespace erstellen

Die weiteren Befehle benötigen eine shell-Variable, in der der name des namespaces angegeben wird. Um Fehler zu vermeiden am besten nur temporär in der aktuellen shell setzen, so wird später nicht aus versehen der falsche namespace verwendet!

    FOLIONS=<NEW_NAMESPACE>
    kubectl create namespace ${FOLIONS:-undefined}


#### Gitlab-token im Namespace installieren

Wenn es bereits andere folio installationen im cluster gibt, secret `gitlab-folio-group` kopieren und bereinigen (siehe Beispiel in `other/gitlab.secret.example.yaml`, vor allem Zeile `namespace:` entfernen):

    kubectl -n <source-namespace> get secret gitlab-folio-group -o yaml > other/gitlab-folio-group.secret.yaml
    vi other/gitlab-folio-group.secret.yaml
    kubectl -n ${FOLIONS:-undefined} apply -f other/gitlab-folio-group.secret.yaml

Für ein neues secret (nach Anlegung im gitlab mit registry-read Berechtigung):

    kubectl -n ${FOLIONS:-undefined} create secret docker-registry gitlab-folio-kubernetes --docker-server=gitlab.bib-bvb.de:5050/0000000001577196/folio-kubernetes --docker-username=k8s-read --docker-password=<token>


#### global-values.yaml anpassen

In der Datei `global-values.yaml` werden Einstellungen vorgenommen, die in mehreren Helm-Repositories relevant sind.
Beispiel Datenbank: 
Benötige Accounts werden hier angegeben, um dann bei der Installation von Postgres erstellt zu werden. 
Beim Installieren von Okapi und Folio-Modulen werden dann die gleichen Daten verwendet.
So gibt es nur eine zentrale Stelle, wo entsprechende Variablen gesetzt werden müssen.



## Installation Basiskomponenten

#### Elasticsearch und Kafka aus bitnami-repo
Einmal pro client-PC: bitnami repo hinzufügen:

    helm repo add bitnami https://charts.bitnami.com/bitnami

Elasticsearch installieren, der `NOTES.txt` output wird mit folgendem Befehl unter `logs/` gespeichert und enthält alle wichtigen informationen:

    helm -n ${FOLIONS:-undefined} install bitnami-elasticsearch bitnami/elasticsearch -f bitnami/elastic-values.yml > logs/elastic-notes-${FOLIONS:-undefined}.log

Wichtig: im outputlog nachschaun, unter welchem internen service elasticsearch zu erreichen ist und mit den values von mod-search abgleichen!
Hier gab es bei bitnami chart upgrades in der Vergangenheit Änderungen.

Wie beim letzten Punkt auch Kafka installieren:

    helm -n ${FOLIONS:-undefined} install bitnami-kafka bitnami/kafka -f bitnami/kafka-values.yml > logs/kafka-${FOLIONS:-undefined}.log

Dann am besten warten, bis im Dashboard alle pods grün sind.


#### postgres 

postgres wird über kubegres installiert, mehr unter `https://www.kubegres.io/doc/getting-started.html`

Eine direkte Installation mit helm führt mit dem Kubegres-Operator zu Problemen, wenn man nachträglich an der Pod-Konfiguration etwas ändern will (konkret: nötige Anpassungen bezüglich shared memory volume sind nach helm upgrade nicht durchgeschlagen). 
Trotzdem sind Templates nütlich, um Variablen nur einmal in `global-values.yaml` setzen zu müssen.
Daher ist die aktuelle Strategie, die Helm-templates zu rendern und dann per `kubectl apply -f ...` manuell zu installieren.
Mehr dazu unter `kubegres/README.md`.


## Installation Folio


#### okapi

Zum Thema Docker-Container und CI, siehe `https://gitlab.bib-bvb.de/folio/okapi`.

    helm -n ${FOLIONS:-undefined} install okapi okapi/ -f global-values.yaml --debug > logs/okapi-install-${FOLIONS:-undefined}.yaml


#### auth-modules
Man braucht zum installieren der Folio-Module mindestens 2 value files:
- `<RELEASE_TAG>-auth.yaml` für die zur Absicherung nötigen Module mod-users, mod-authtoken, mod-permissions und mod-login
- `<RELEASE_TAG>-no-auth.yaml>` für alle anderen Module eines Folio-Releases.

Zusätliche Module können auch in einem seperaten Helm-Release

Bei einem neuen Folio-Release kannen das Skript `modules/script/update-helm-module-values.py` verwendet werden.

Die zur Authentifizierung nötigen Module werden in einem seperaten Helm-Release installiert, um beim Komplikationen beim Upgraden zu vermeiden:

    helm -n ${FOLIONS:-undefined} install mods-auth modules/ -f global-values.yaml -f modules/valuesd/<RELEASE_TAG>-auth.yaml --debug > logs/mods-auth-install-${FOLIONS:-undefined}.yaml


#### secure supertenant

Per Skript Folio-supertenant mit den für die Authorisierung nötigen Modulen versorgen und absichern. Der Job wird per Helm template erzeugt, es sollte keine Konfiguration nötig sein (bei gleicher `global-values.yaml`).

    helm -n ${FOLIONS:-undefined} template secure secure-supertenant/ -f global-values.yaml > secure-supertenant/renders/secure-supertenant.yaml
    kubectl -n ${FOLIONS:-undefined} apply -f secure-supertenant/renders/secure-supertenant.yaml


#### andere modules

Die restlichen Module werden sperat im schon abgesicherten System nachinstalliert. 
So kann man diese Installation per `helm upgrade` relative leicht um weitere Module erweitern und Einstellungen der Module ändern.

    helm -n ${FOLIONS:-undefined} install mods modules/ -f global-values.yaml -f modules/valuesd/<RELEASE_TAG>-no-auth.yaml --debug > logs/mods-install-${FOLIONS:-undefined}.yaml


#### tenant erstellen und stripes deployment
- erstelle neue stripes branch in gitlab ( https://gitlab.bib-bvb.de/folio/platform-complete ) aus branch mit richtiger version (z.B. lotus-base) und Werte anpassen:
  - unter docker/Dockerfile die ARGS an: OKAPI\_URL auf gewünschten ingress und TENANT\_ID
  - in `stripes.config.js` logo/favicon und Beschreibung anpassen. URL/tenant in dieser Datei werden von den ARGS der Dockerfile überschrieben.
  - docker image bauen (manuell auf eigenem Arbeitsrechner und dann pushen, oder einen git tag vergeben und pushen)
- tenant/values-example.yaml kopieren und anpassen, wichtig: 
  - tenantId
  - stripes.imageVersion   (auf branchname)
  - folgende 3 variablen können gleich sein, und sollten zum ingress der in der stripes Dockerfile angegeben wurde passen:
    - ingress.host
    - ingress.tls.secretName
    - ingress.tls.host
- Installieren in neuem Helm Release, bei nur einem tenant kann dessen id genutzt werden 

Befehl Beispiel:

    helm -n ${FOLIONS:-undefined} install <RELEASENAME> tenant/ -f global-values.yaml -f tenant/values-<RELEASENAME>.yaml -f modules/valuesd/<RELEASE_TAG>-all.yaml --debug > tenant/install-debug-${FOLIONS:-undefined}.yml


#### Notizen zum deinstallieren

Per `helm -n ${FOLIONS:-undefined} list` installierte Releases checken

Per `helm -n ${FOLIONS:-undefined} uninstall <RELEASENAME>` deinstallieren

Zu beachten: 
- Persistent Volume Claims von Elasticsearch, Kafka und Kubegres werden nicht automatisch gelöscht, muss manuell erledigt werden.
- Secrets mit Kubegres und Supertenant Passwörtern werden nicht automatisch gelöscht und beim neu installieren wieder verwendet. Sollte das nicht gewünscht sein, manuell deinstallieren oder Namespace löschen.
