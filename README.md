# DISCLAIMER

THIS REPOSITORY IS UNMAINTAINED. See here for current development:
https://gitlab.bib-bvb.de/folio-public/folio-helm


# folio-helm

Prototype of Helm-based installation and configuration of folio and base components (elasticsearch, kafka, postgres) in Kubernetes at LRZ.

For Basic Usage see [USAGE.md](USAGE.md) (german for now and mainly for internal use, but there are several commands which could be helpful/provide an idea how to use this project. Might translate in the future!)

Quite a lot of the setup depends on the infrastructure and Cluster Setup (storage, ingress, DNS/URLs etc.)

The goal of this project is:
* automation to prevent human error
* maintainability --> heavy use of templating and global values file
* flexibility (Multitenant!) --> multiple Helmcharts

Will probably be added in the future:
* better/more clear scripts for module and tenant registration with Okapi
* procedures for upgrades (folio, tenants, base components)
* scripts to update module value files to new versions (already in development)

Dockerfiles of self-built containers hosted internally at LRZ located in [docker/](docker/) (may be incomplete, e.g. for stripes frontend see [platform-complete](https://github.com/folio-org/platform-complete))

STILL IN DEVELOPMENT! Might contain old and/or unneeded scripts/variables/files.

For feedback/suggestions/questions contact Florian.Kreft@lrz.de or @Florian_Kreft in [folio slack](https://folio-project.slack.com/)
